const express = require('express')
const app = express()
const http = require('http').Server(app)
const path = require('path')
const io = require('socket.io')(http)

app.use(express.static('./public'))

app.get('/', (req, res) => {
  res.sendFile(path.resolve('./index.html'))
})

io.on('connection', socket => {
  console.log('a user connected')
  socket.on('disconnect', () => {
    console.log('user disconnected')
  })
  socket.on('chat message', msg => {
    console.log('message: ' + msg)
    // io.emit('chat message', msg)
    socket.broadcast.emit('chat message', msg)
  })
})

http.listen(3000, () => {
  console.log('express listening on localhost:3000')
})
